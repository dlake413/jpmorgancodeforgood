<?php

//DAO.php 
//Include all the data access object here
//These are to make the query call much easier so that we don't have to have query statement all over the web.
include_once("db.php");

function createVolunteer($L_NAME,$F_NAME,$EMAIL,$PASSWORD,
			 $ZIP,$GENDER,$DOB,$RANGE,$PD_STATUS, $PARTICIPATE_TRAILS=''){
  $db = connectmysqli();
  $stmt = $db->stmt_init();

  $PASSWORD = md5($PASSWORD);
  
  if($stmt->prepare("INSERT INTO VOLUNTEERS (L_NAME, F_NAME, EMAIL, PASSWORD, ZIP, GENDER, DOB, MILE_RANGE, PD_STATUS, PARTICIPATE_TRIALS) VALUES (?,?,?,?,?,?,?,?,?,?)")){
    $stmt->bind_param("ssssississ", $L_NAME,$F_NAME,$EMAIL, $PASSWORD, $ZIP, $GENDER, $DOB, $RANGE, $PD_STATUS, $PARTICIPATE_TRAILS);
    
    if($stmt->execute()){
      return $db->insert_id;
    } 
    else {
      return false;
    }
  }
  else {
    echo "error";
    return false;
  }
}

//readVolunteers will read users by email address

function readVolunteers($email='')
{
  $db = connectmysqli();
  $stmt = $db->stmt_init();

  $email = "%".$email."%";

  if($stmt->prepare("SELECT * FROM VOLUNTEERS WHERE EMAIL LIKE ?")){
    $stmt->bind_param("s", $email);
    $stmt->execute();
    $stmt->bind_result($VOL_ID, $L_NAME,$F_NAME,$EMAIL, $PASSWORD, $ZIP, $GENDER, $DOB, $RANGE, $PD_STATUS, $PARTICIPATE_TRAILS);

    $resultArray = array();

    while($stmt->fetch()){
      $row = array(
	"VOL_ID" => $VOL_ID,
	"L_NAME" => $L_NAME,
	"F_NAME" => $F_NAME,
	"EMAIL" => $EMAIL, 
	"PASSWORD" => $PASSWORD, 
	"ZIP" => $ZIP, 
	"GENDER" => $GENDER, 
	"DOB" => $DOB, 
	"RANGE" => $RANGE, 
	"PD_STATUS" => $PD_STATUS, 
	"PARTICIPATE_TRAILS" => $PARTICIPATE_TRAILS
      );
      
      $resultArray[] = $row;
    }
    return $resultArray;
  }
}

//readVolunteer by VOL_ID
function readVolunteer($VOL_ID){
  $db = connectmysqli();
  $stmt = $db->stmt_init();

  if($stmt->prepare("SELECT * FROM VOLUNTEERS WHERE VOL_ID = ?")){
    $stmt->bind_param("i", $VOL_ID);
    $stmt->execute();
    $stmt->bind_result($VOL_ID, $L_NAME,$F_NAME,$EMAIL, $PASSWORD, $ZIP, $GENDER, $DOB, $RANGE, $PD_STATUS, $PARTICIPATE_TRAILS);

    $resultArray = array();

    $stmt->fetch();
    $row = array(
      "VOL_ID" => $VOL_ID,
      "L_NAME" => $L_NAME,
      "F_NAME" => $F_NAME,
      "EMAIL" => $EMAIL, 
      "PASSWORD" => $PASSWORD, 
      "ZIP" => $ZIP, 
      "GENDER" => $GENDER, 
      "DOB" => $DOB, 
      "RANGE" => $RANGE, 
      "PD_STATUS" => $PD_STATUS, 
      "PARTICIPATE_TRAILS" => $PARTICIPATE_TRAILS
    );
      
    return $row;
  }
  return false;
}

function createTrial($NAME, $DESCRIPTION, $ACCEPT_CONTROL, $MIN_AGE, $MAX_AGE,
		     $YEARS_SINCE_DIAG, $STUDY_ID, $FOCUS_ID, $SPONSER_NAME,
		     $STREET, $CITY, $STATE, $ZIP)
{
  
}

function readTrials($age, $PD){

  $db = connectmysqli();
  $stmt = $db->stmt_init();

  $query = "SELECT TRIAL_ID, NAME, STREET, CITY, STATE, ZIP, lng, lat 
	    FROM TRIAL WHERE MIN_AGE <= ? AND MAX_AGE >= ?";
  if($PD == 'N'){
    $query = $query." AND ACCEPT_CONTROL = ?";
    $stmt->prepare($query);
    $accept_control = 'Y';
    $stmt->bind_param("iis", $age, $age, $accept_control);
  } else {
    $stmt->prepare($query);
    $stmt->bind_param("ii", $age, $age);
  }

  $stmt->execute();

  $stmt->bind_result($TRIAL_ID, $NAME, $STREET, $CITY, $STATE, $ZIP, $lng, $lat);

  $result = array();

  while($stmt->fetch()){
    $row = array(
      "TRIAL_ID" => $TRIAL_ID,
      "NAME" => $NAME,
      "STREET" => $STREET,
      "CITY" => $CITY,
      "STATE" => $STATE,
      "ZIP" => $ZIP,
      "lng" => $lng,
      "lat" => $lat
    );
    $result[] = $row;
  }
  return $result;
}

function createPD($VOL_ID, $CONDITION_ID, $DATE_DIAGNOSED, $DATE_START_MED, $DATE_SYMPTOMS){
  $db = connectmysqli();
  $stmt = $db->stmt_init();

  if($stmt->prepare("INSERT INTO PD VALUES (?,?,?,?,?)")){
    $stmt->bind_param('iisss', $VOL_ID, $CONDITION_ID, $DATE_DIAGNOSED, $DATE_START_MED, $DATE_SYMPTOMS);
    $stmt->execute();
  } else {
    return false;
  }
}

function createVOL_MEDS($VOL_ID, $MED_ID, $TIME_ID){
  $db = connectmysqli();
  $stmt = $db->stmt_init();

  if($stmt->prepare("INSERT INTO VOL_MEDS VALUES (?,?,?)")){
    $stmt->bind_param('iii', $VOL_ID, $MED_ID, $TIME_ID);
    $stmt->execute();
  } else {
    return false;
  }
}

function createVOL_PROCEDURES($VOL_ID, $PROCEDURE_ID){
  $db = connectmysqli();
  $stmt = $db->stmt_init();
  
  if($stmt->prepare("INSERT INTO VOL_PROCEDURES VALUES (?,?)")){
    $stmt->bind_param('ii', $VOL_ID, $PROCEDURE_ID);
    $stmt->execute();
  } else {
    return false;
  }
}

function createVOL_SUPPLEMENTS($VOL_ID, $SUPPLEMENT_ID, $TIME_ID){
  $db = connectmysqli();
  $stmt = $db->stmt_init();
  if($stmt->prepare("INSERT INTO VOL_SUPPLEMENTS VALUES (?,?,?)")){
    $stmt->bind_param('iii', $VOL_ID, $SUPPLEMENT_ID, $TIME_ID);
    $stmt->execute();
  } else {
    return false;
  }
}

?>