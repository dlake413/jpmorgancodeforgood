<!--Asks user which medications they have taken and when -->
<?php
include_once("../settings/DAO.php");
session_start();
$folder=$_SESSION['folder'];
if($_SESSION['permission'] == 0){
	header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Login.php');
}
$vol_id = $_SESSION['vol_id'];
$a = $_POST['a'];
$b = $_POST['b'];
$c = $_POST['c'];
$d = $_POST['d'];
$e = $_POST['e'];
$f = $_POST['f'];
$g = $_POST['g'];
$h = $_POST['h'];
$i = $_POST['i'];
$j = $_POST['j'];
$k = $_POST['k'];
$l = $_POST['l'];
$m = $_POST['m'];
$n = $_POST['n'];
$o = $_POST['o'];
$p = $_POST['p'];
$q = $_POST['q'];
?>

<!--
		Question 5
-->

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

  <!-- Load bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "css/bootstrap.css" rel = "stylesheet">
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
    <script src = "js/bootstrap.js"></script>

<style>
  .pic{
    margin: auto;
    display: inline-block;
    text-align: left;
    font-size: 12pt;
    width: 300px;
  }
</style>

</head>

<div class = "logo" align="center">
<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
</div>
<BR>
<body>
<center>
<div class = "pic">
<form action="FoxTrialFinder.php" method="post">

Are you taking or have you taken any of these medications? <BR><BR>
C = Currently Taking  <BR>
T = Taken in Past <BR>
N = Never Taken <BR><BR>


 Amantadine (Symmetrel)  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="a" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="a" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="a" value="3" required> N
    </label>
</div>
<BR>

 Apomorphine (Apokyn)    <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="b" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="b" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="b" value="3" required> N
    </label>
</div>
<BR>
 Benztropine (Cogentin)    <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="c" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="c" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="c" value="3" required> N
    </label>
</div>
<BR>
   Bromocriptine (Parlodel)       <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="d" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="d" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="d" value="3" required> N
    </label>
</div>
<BR>
  Carbidopa, levodopa, and entacapone (Stalevo)       <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="e" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="e" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="e" value="3" required> N
    </label>
</div>
<BR>
  Carbidopa-levodopa (Sinemet)      <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="f" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="f" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="f" value="3" required> N
    </label>
</div>
<BR>
 Duodopa    <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="g" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="g" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="g" value="3" required> N
    </label>
</div>
<BR>
Entacapone (Comtan)          <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="h" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="h" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="h" value="3" required> N
    </label>
</div>
<BR>
 Levodopa-benserazide (Madopar)    <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="i" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="i" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="i" value="3" required> N
    </label>
</div>
<BR>
  Melevodopa (Sirio)   <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="j" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="j" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="j" value="3" required> N
    </label>
</div>
<BR>
 Pramipexole (Mirapex, Mirapex ER, Mirapexin, Sifrol)         <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="k" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="k" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="k" value="3" required> N
    </label>
</div>
<BR>
 Rasagiline (Azilect)    <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="l" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="l" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="l" value="3" required> N
    </label>
</div>
<BR>
 Ropinirole (Adartel, Requip, Requip XL, Ropark)  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="m" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="m" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="m" value="3" required> N
    </label>
</div>
<BR>
 Rotigotine (Neupro) <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="n" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="n" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="n" value="3" required> N
    </label>
</div>
<BR>
  Selegiline (I-deprenyl, Eldepryl, Zelapar)  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="o" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="o" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="o" value="3" required> N
    </label>
</div>
<BR>
 Tolcapone (Tasmar)    <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="p" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="p" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="p" value="3" required> N
    </label>
</div>
<BR>
  Trihexyphenidyl (Apo-Trihex, Artane)        <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="q" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="q" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="q" value="3" required> N
    </label>
</div>
<BR>
<input type="submit" value="Finish" name="finish" class="btn-lg btn-default">
</form>
<?php
	createVOL_MEDS($vol_id, 1, $a);
	createVOL_MEDS($vol_id, 2, $b);
	createVOL_MEDS($vol_id, 3, $c);
	createVOL_MEDS($vol_id, 4, $d);
	createVOL_MEDS($vol_id, 5, $e);
	createVOL_MEDS($vol_id, 6, $f);
	createVOL_MEDS($vol_id, 7, $g);
	createVOL_MEDS($vol_id, 8, $h);
	createVOL_MEDS($vol_id, 9, $i);
	createVOL_MEDS($vol_id, 10, $j);
	createVOL_MEDS($vol_id, 11, $k);
	createVOL_MEDS($vol_id, 12, $l);
	createVOL_MEDS($vol_id, 13, $m);
	createVOL_MEDS($vol_id, 14, $n);
	createVOL_MEDS($vol_id, 15, $o);
	createVOL_MEDS($vol_id, 16, $p);
	createVOL_MEDS($vol_id, 17, $q);
	if(isset($_POST['finish'])){ 
		echo "hello";
		header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/FoxTrialFinder.php');
	}
	
?>






</div>
</center> 

</body>

</html>