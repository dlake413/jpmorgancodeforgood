<?php
session_start();
//ini_set('display_errors',1);
//error_reporting(-1);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register Now!</title>

		<!-- Load bootstrap -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "css/bootstrap.css" rel = "stylesheet">
		<link href = "css/styles.css" rel = "stylesheet">

		<script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src = "js/bootstrap.js"></script>

</head>
<style>

/*	 
	.logo{
		
		width: 500px
	}
*/
	.pic{
		margin: auto;
    display: inline-block;
		text-align: left;
		font-size: 12pt;
		width: 300px;
	}
</style>
<center>
<div class = "logo" align="center">
<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
</div>
<BR>
<?php
include_once("../settings/DAO.php");
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$PD = $_POST['PD'];
$ZIP = $_POST['ZIP'];
$distance = $_POST['distance'];
$email = $_POST['email'];
$password = $_POST['password'];
$password2 = $_POST['password2'];
?>


<body>

<div class="pic">
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" 
class="navbar-form navbar-left">
  <div class="form-group">
    First Name: <input type="text" class="form-control" placeholder="First Name" 
    name = "firstname" value = "<?echo $firstname ?>">
  </div>
  	<BR>
   <div class="form-group">
    Last Name: <input type="text" class="form-control" placeholder="Last Name" 
    name = "lastname" value = "<?echo $lastname ?>">
  </div>
	<BR>
  <div class="form-group">
    Date of Birth: <input type="date" class="form-control"
    name = "dob" value = "<?echo $dob ?>">
  </div>
	<BR>
	Gender:
  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="gender" value="male"> Male
    </label>
    <label class="btn btn-default">
      <input type="radio" name="gender" value="female"> Female
    </label>
  </div>
   <BR>
   Have you been diagnosed with Parkinsons disease?
  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="PD" value="Y"> Yes
    </label>
    <label class="btn btn-default">
      <input type="radio" name="PD" value="N"> No
    </label>
  </div>
 <BR>
 <div class="form-group">
    Zip Code <input type="text" class="form-control" placeholder="Zip Code" 
    name = "ZIP" value = "<?echo $ZIP ?>">
  </div>
<BR>
  How many miles are you willing to travel for a clinical trial? 
  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="distance" value="10" > 10
    </label>
    <label class="btn btn-default">
      <input type="radio" name="distance" value="25"> 25
    </label>
    <label class="btn btn-default">
      <input type="radio" name="distance" value="50"> 50
    </label>
    <label class="btn btn-default">
      <input type="radio" name="distance" value="150"> 150
    </label>
  </div>
  <BR>
  <div class="form-group">
    Email: <input type="email" class="form-control" placeholder="Email" 
    name = "email" value = "<?echo $email ?>">
  </div>
<BR>
  <div class="form-group">
   Password: <input type="password" class="form-control" placeholder="Password" 
    name = "password">
  </div>
  <BR>
  <div class="form-group">
   Re-Enter Password: <input type="password" class="form-control" placeholder="Re-Enter Password" 
    name = "password2">
  </div>
<BR><BR>
<input type="submit" value="Register Now" name="register" class="btn-lg btn-default"  /><BR><BR>
</form>
</div>
<?php
$folder="/testfiles";
$_SESSION['folder'] = $folder;
if(isset($_POST['register'])){ 
	if($password != $password2){
		echo "Passwords must match";
	}
	//else if($firstname == '' || $lastname == '' || $dob == '' || $gender == '' || $PD == '' || $ZIP =='' || $distance == '' || $email == '' || $password == '' || $password2 == ''){
	//	echo "Please complete all fields in the form above";
	//}
	else{
		$vol_id = createVolunteer($lastname, $firstname, $email, $password, $ZIP, $gender, $dob, $distance, $PD);

		if(vol_id == FALSE){
			echo "INSERT ERROR";
		}
		else{
			echo "in else";
			$_SESSION['vol_id'] = $vol_id;
			$_SESSION['PD'] = $PD;
			$age = floor((strtotime(date('d-m-Y')) - strtotime($dob))/(60*60*24*365.2421896));
  			$_SESSION['age']=$age;
			//header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com/'.$folder.'/PreliminaryResults.php');
		}
	}
}


?>

</center>
</body>

</html>