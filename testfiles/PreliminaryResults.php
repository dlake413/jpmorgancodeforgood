<?php
include_once("../settings/DAO.php");
include_once("geolocation.php");
session_start();
/*
ini_set('display_errors',1);
error_reporting(-1);
*/


//Comparator for the Trial instances, comparing two instances by the distance.

function distanceComparator($a, $b){
  if($a['distance'] == $b['distance']) return 0;
  return ($a['distance'] < $b['distance'])?-1:1;
}


//input AGE, PD, Traval Miles, ZIP code
//Finds all Trials within range of $mile from $zip, respect to $age.

function findReleventTrials($age, $PD, $mile, $zip){

  //userLoc, where is user at in Lat / Long?
  $userLoc = getLatLong(null, $zip);
  $Trials = readTrials($age, $PD);

  $results = array();

  foreach ($Trials as $trial){
    $distance = calculateDistance($trial['lat'],$trial['lng'],$userLoc['lat'],$userLoc['lng']);
    if($distance <= $mile){
      $row = array(
	"distance" => $distance,
	"trial" => $trial
      );
      
      $results[] = $row;
    }

  }

  usort($results, "distanceComparator");

  $results[] = $userLoc;
  return $results;
}

?>