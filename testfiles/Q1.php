<!-- Gets current condition and date diagnosed-->
<?php
include_once("../settings/DAO.php");
session_start();
$vol_id = $_SESSION['vol_id'];
$folder=$_SESSION['folder'];
if($_SESSION['permission'] == 0){
	header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Login.php');
}
$date = $_POST['date'];
$condition = $_POST['condition'];
?>
<!DOCTYPE html>
<html>
<head>

<div class = "logo" align="center">
<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
</div>

	
<meta charset="UTF-8">
<title>Date Diagnosed</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "css/bootstrap.css" rel = "stylesheet">
		<script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
    	<script src = "js/bootstrap.js"></script>
</head>
<style>

/*
	Creating a Block to center the text together
	*/

  .pic{
    margin: auto;
    display: inline-block;
    text-align: left;
    font-size: 12pt;
    width: 300px;
  }
</style>

<br>
<body>
<center>
<div class = "pic">
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
 <div class="form-group">
    Date Diagnosed: <input type="date" class="form-control"
    name = "date" value = "<?echo $date ?>" required>
  </div>
<BR>
<div class="btn-group btn-toggle" data-toggle="buttons">
	Choose the best description for your current condition: <BR>
    <label class="btn btn-default">
      <input type="radio" name="condition" value="1" required> No PD symptoms or complaints
    </label>
    <label class="btn btn-default">
		<input type="radio" name="condition" value="2" required> PD symptoms on one side of the body
    </label>
    <label class="btn btn-default">
		<input type="radio" name="condition" value="3" required> PD symptoms on both sides of the body
    </label>
    <label class="btn btn-default">
		<input type="radio" name="condition" value="4" required> Impaired balance, but still independent
    </label>
    <label class="btn btn-default">
		<input type="radio" name="condition" value="5" required> Severely disabled, but able to walk unassisted
    </label>
    <label class="btn btn-default">
		<input type="radio" name="condition" value="6" required> Wheelchair or bedridden unless assisted
    </label>
  </div>
  <br>
<BR><input type="submit" value="Save and Exit" name="save" class="btn-lg btn-default"> <input type="submit" value="Next" name="next" class="btn-lg btn-default">
</form>

<?php
	createPD($vol_id, $condition, $date, '', '');
	if(isset($_POST['next'])){ 
		header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Q2.php');
	}
	if(isset($_POST['save'])){ 
		header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/FoxTrialFinder.php');
	}
?>
</div>
</center>
</body>

</html>