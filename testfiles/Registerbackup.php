<?php
session_start();
//ini_set('display_errors',1);
//error_reporting(-1);
include_once("../settings/DAO.php");
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$PD = $_POST['PD'];
$ZIP = $_POST['ZIP'];
$distance = $_POST['distance'];
$email = $_POST['email'];
$password = $_POST['password'];
$password2 = $_POST['password2'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register Now!</title>

		<!-- Load bootstrap -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "css/bootstrap.min.css" rel = "stylesheet">
		<link href = "css/styles.css" rel = "stylesheet">

		<script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src = "js/bootstrap.js"></script>

</head>
<body>
<center>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
First Name <input type="text" name = "firstname" value = "<?echo $firstname ?>"><BR>
Last Name <input type="text" name = "lastname" value = "<?echo $lastname ?>"><BR>
Date of Birth <input type="date" name = "dob" value = "<?echo $dob ?>"><BR>
Gender: M <input type="radio" name = "gender" value="male"> F <input type="radio" name = "gender" value="female"> <BR>
Have you been diagnosed with Parkinsons disease? Yes <input type="radio" name = "PD" value="Y"> No <input type="radio" name = "PD" value="N"><BR>
ZIP Code <input type="text" name = "ZIP" value = "<?echo $ZIP ?>"><BR>
How far are you willing to travel for a clinical trial? 
<select name="distance">
  <option value="">Select...</option>
  <option value="300">300 miles</option>
  <option value="150">150 miles</option>
  <option value="50">50 miles</option>
  <option value="25">25 miles</option>
</select> <BR>
Email <input type="email" name = "email" value = "<?echo $email ?>"><BR>
Password <input type="password" name = "password"><BR>
Re-Enter Password <input type="password" name = "password2"><BR>
<BR><input type="submit" value="Register" name="register"><BR><BR>
</form>

<?php
$folder="/testfiles";
$_SESSION['folder'] = $folder;
if(isset($_POST['register'])){ 
	if($password != $password2){
		echo "Passwords must match";
	}
	else if($firstname == '' || $lastname == '' || $dob == '' || $gender == '' || $PD == '' || $ZIP =='' || $distance == '' || $email == '' || $password == '' || $password2 == ''){
		echo "Please complete all fields in the form above";
	}
	else{
		$vol_id = createVolunteer($lastname, $firstname, $email, $password, $ZIP, $gender, $dob, $distance, $PD);
		if(vol_id == FALSE){
			echo "INSERT ERROR";
		}
		else{
			$_SESSION['range'] = $distance;
			$_SESSION['vol_id'] = $vol_id;
			$_SESSION['PD'] = $PD;
			$age = floor((strtotime(date('d-m-Y')) - strtotime($dob))/(60*60*24*365.2421896));
  			$_SESSION['age']= $age;
			header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com/'.$folder.'/PreliminaryResults.php');
		}
	}
}


?>
</center>

</body>

</html>