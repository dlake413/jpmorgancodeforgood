<!-- This page gets name, email, password, if the user has parkinsons, gender, 
DOB, range they are willing to travel for a trial, then create a volunteer based on 
this information. Send information to -->
<?php
session_start();
//If user is registering they are automatically logged in when they register
$_SESSION['permission'] = 1;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register Now!</title>
		<!-- Load bootstrap -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "css/bootstrap.css" rel = "stylesheet">
		<link href = "css/styles.css" rel = "stylesheet">

		<script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
		<script src = "js/bootstrap.js"></script>
</head>
<style>
	.pic{
		margin: auto;
    	display: inline-block;
		text-align: left;
		font-size: 12pt;
		width: 300px;
	}
</style>
<center>
<!-- Fox Trial Finder logo -->
<div>
<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
</div>
<BR>
<?php
//Including functions for mySQL
include_once("../settings/DAO.php");

//Keeping variables so they can display in form in case of error
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$PD = $_POST['PD'];
$ZIP = $_POST['ZIP'];
$distance = $_POST['distance'];
$email = $_POST['email'];
$password = $_POST['password'];
$password2 = $_POST['password2'];
$message = "Just 10 Questions to Answer!";
?>

<body>

<h4>
<?php
  echo $message;
?>
</h4>

<!-- Form for entering basic registration info -->
<div class="pic">
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" 
class="navbar-form navbar-left">
  <div class="form-group">
    First Name: <input type="text" class="form-control" placeholder="First Name" 
    name = "firstname" value = "<?echo $firstname ?>" required>
  </div>
  	<BR>
   <div class="form-group">
    Last Name: <input type="text" class="form-control" placeholder="Last Name" 
    name = "lastname" value = "<?echo $lastname ?>" required>
  </div>
	<BR>
  <div class="form-group">
    Date of Birth: <input type="date" class="form-control"
    name = "dob" value = "<?echo $dob ?>" required>
  </div>
	<BR>
	Gender:
  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="gender" value="male" required> Male
    </label>
    <label class="btn btn-default">
      <input type="radio" name="gender" value="female" required> Female
    </label>
  </div>
   <BR>
   Have you been diagnosed with Parkinsons disease?
  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="PD" value="Y" required> Yes
    </label>
    <label class="btn btn-default">
      <input type="radio" name="PD" value="N" required> No
    </label>
  </div>
 <BR>
 <div class="form-group">
    Zip Code <input type="text" class="form-control" placeholder="Zip Code" 
    name = "ZIP" value = "<?echo $ZIP ?>" required>
  </div>
<BR>
  How many miles are you willing to travel for a clinical trial? 
  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="distance" value="10" required> 10
    </label>
    <label class="btn btn-default">
      <input type="radio" name="distance" value="25" required> 25
    </label>
    <label class="btn btn-default">
      <input type="radio" name="distance" value="50" required> 50
    </label>
    <label class="btn btn-default">
      <input type="radio" name="distance" value="150" required> 150
    </label>
  </div>
  <BR>
  <div class="form-group">
    Email: <input type="email" class="form-control" placeholder="Email" 
    name = "email" value = "<?echo $email ?>" required>
  </div>
<BR>
  <div class="form-group">
   Password: <input type="password" class="form-control" placeholder="Password" 
    name = "password" id="inputPassword" required>
  </div>
  <BR>
  <div class="form-group">
   Re-Enter Password: <input type="password" class="form-control" placeholder="Re-Enter Password" 
    name = "password2" required>
  </div>
<BR>

<?php
//Keep folder in case we move code from testfiles folder
$folder="/testfiles";
$_SESSION['folder'] = $folder;

//If the register button is clicked
if(isset($_POST['register'])){ 
	//If the passwords do not match
  if($password != $password2){
    echo "Error, the passwords must match";
  }
  else{
    $vol_id = createVolunteer($lastname, $firstname, $email, $password, $ZIP, $gender, $dob, $distance, $PD);
    //If the email address does not exist
    if($vol_id == FALSE){
      $message = "INSERT ERROR";
    }
    else{
    	//Save vol_id, age, range, and if user has parkinsons as session variables to use on next page
      $_SESSION['vol_id'] = $vol_id;
      $_SESSION['PD'] = $PD;
      $age = floor((strtotime(date('d-m-Y')) - strtotime($dob))/(60*60*24*365.2421896));
    	$_SESSION['age']=$age;
    	$_SESSION['range']=$distance;
    	$_SESSION['ZIP']=$ZIP;
    	//Jump to preliminary.php site - will show trials that match based on age, PD status, location, and range
      header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/preliminary.php');
    }
  }
}
?><BR><BR>
<input type="submit" value="Register Now" name="register" class="btn-lg btn-default"  /><BR><BR>
</form>
</div>

</center>
</body>
</html>