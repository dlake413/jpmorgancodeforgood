<!--Home page, allows login with existing username and pw or registration--> 
<?php
session_start();
include_once("../settings/DAO.php");
$_SESSION['permission'] = 0;
$_SESSION['folder'] = "/testfiles";
$folder=$_SESSION['folder'];
if($_POST['email'] != ''){
	$email = $_POST['email'];
}
else{
	$email = "Email";
}
$password = $_POST['password'];
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Fox Trial Finder Log In</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "css/bootstrap.css" rel = "stylesheet">
		<link href = "css/styles.css" rel = "stylesheet">

	</head>

	<div class = "logo" align="center">
	<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
	</div>

	<body>
	
	<br>
	<div class="container">
		<div class="row">	
			<div class="col-md-4"></div>
			<form class="col-md-4" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
			    <div class="form-group">
			        <input type="text" class="form-control input-lg" placeholder="<? echo $email ?>" name="email">
			    </div>
			    <div class="form-group">
			        <input type="password" class="form-control input-lg" placeholder="Password" name="password">
			    </div>
			    <div class="form-group">
			        <input type="submit" class="btn btn-default btn-lg btn-block" value="Sign In" name="signin">
			        <br>
			        <input type="submit" class="btn btn-default btn-lg btn-block" value="Register Here!" name="register">
					<br>
					<input type="submit" class="btn btn-default btn-lg btn-block" value="Quick Register - Email Only!" name="qregister">
			    </div>
			</form>
			<div class="col-md-4"></div>
		</div>
	</div>
	
		
		<script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src = "js/bootstrap.js"></script>
		<center>
		<?php
			if(isset($_POST['signin'])){
				$vol_array = readVolunteers($email);
				if(count($vol_array) == 0){
					echo "Invalid email address";}
				else if(md5($password) != $vol_array[0]['PASSWORD']){
					echo "Username and password do not match";
				}
				else{
					$_SESSION['vol_id'] = $vol_array[0]['VOL_ID'];
					$_SESSION['permission'] = 1;
					header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/FoxTrialFinder.php');
				}
			}
			if(isset($_POST['register'])){
				header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Register.php');
			}
			if(isset($_POST['qregister'])){
				header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/quickreg.php');
			}
		?>
		</center>
	</body>
	
</html>