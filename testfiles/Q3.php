<?php
include_once("../settings/DAO.php");
session_start();
$folder=$_SESSION['folder'];
if($_SESSION['permission'] == 0){
	header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Login.php');
}
$vol_id = $_SESSION['vol_id'];
$CoQ10 = $_POST['CoQ10'];
$Creatine = $_POST['Creatine'];
$Inosine = $_POST['Inosine'];
$VitaminC = $_POST['VitaminC'];
$VitaminD = $_POST['VitaminD'];
$VitaminE = $_POST['VitaminE'];
?>
<!-- 
	Question 4 (suppliments)
-->
<!DOCTYPE html>
<html>
<head>

<div class = "logo" align="center">
<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
</div>

<BR>
<meta charset="UTF-8">
<title>Title of the document</title>
 <!-- Load bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href = "css/bootstrap.css" rel = "stylesheet">
    <script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
    <script src = "js/bootstrap.js"></script>
<style>
  .pic{
    margin: auto;
    display: inline-block;
    text-align: left;
    font-size: 12pt;
    width: 300px;
  }
</style>
</head>
<body>
<center>
<div class = "pic">
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
Are you taking or have you taken any of these suppliments? <BR><BR>
C = Currently Taking  <BR>
T = Taken in Past <BR>
N = Never Taken <BR> <BR>
CoQ10 <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
     <input type="radio" name="CoQ10" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="CoQ10" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="CoQ10" value="3" required> N
    </label></div><BR>
    Creatine <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="Creatine" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Creatine" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Creatine" value="3" required> N
    </label> </div> <BR>
    Inosine <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="Inosine" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Inosine" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Inosine" value="3" required> N
    </label> </div><BR>
    Vitamin C <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="Vitamin C" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Vitamin C" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Vitamin C" value="3" required> N
    </label></div> <BR>
    Vitamin D <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="Vitamin D" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Vitamin D" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Vitamin D" value="3" required> N
    </label> </div> <BR>
    Vitamin E  <div class="btn-group-lg btn-toggle" data-toggle="buttons">
    <label class="btn btn-default">
      <input type="radio" name="Vitamin E" value="1" required> C
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Vitamin E" value="2" required> T
    </label>
    <label class="btn btn-default">
      <input type="radio" name="Vitamin E" value="3" required> N
    </label>
</div>
<BR>
<input type="submit" value="Save and Exit" name="save" class="btn-lg btn-default">  <input type="submit" value="Next" name="next" class="btn-lg btn-default">
</form>

<?php
	createVOL_SUPPLEMENTS($vol_id, 1, $CoQ10);
	createVOL_SUPPLEMENTS($vol_id, 2, $Creatine);
	createVOL_SUPPLEMENTS($vol_id, 3, $Inosine);
	createVOL_SUPPLEMENTS($vol_id, 4, $VitaminC);
	createVOL_SUPPLEMENTS($vol_id, 5, $VitaminD);
	createVOL_SUPPLEMENTS($vol_id, 6, $VitaminE);
	if(isset($_POST['next'])){ 
		header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Q4.php');
	}
	if(isset($_POST['save'])){ 
		header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/FoxTrialFinder.php');
	}
?>


</div>
</center> 


</body>

</html>