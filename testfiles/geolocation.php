<?php

function calculateDistance($latitude1, $longitude1, $latitude2, $longitude2) {
  $theta = $longitude1 - $longitude2;
  $miles = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
  $miles = acos($miles);
  $miles = rad2deg($miles);
  $miles = $miles * 60 * 1.1515;
      return $miles; 
}

//input: $address = Associative Array, contains 'st1', 'city', 'state' (OPTIONAL!)
//input: $zip = 5 digit postal-code
//getLatLong will return Associative Array of Long Lat
//return false if no geolocation found.

// $Address input template:
// $array = array(
//   "st1" => "343 Norman Drive",
//   "city" => "Newark",
//   "state" => "DE");

function getLatLong($address, $zip){

  $url = "http://maps.google.com/maps/api/geocode/json?address=";

  if($address == null){
    $url = $url.urlencode($zip)."&sensor=false";
    $result_string = file_get_contents($url);
    $result = json_decode($result_string, true);
    $result1 = $result['results'][0]['geometry']['location'];
    return array('lat' => $result1['lat'],
		 'lng' => $result1['lng']);
  } else {
    $url = $url.urlencode(
      str_replace(' ','+',$address['st1']).',+'.str_replace(' ','+',$address['city']).',+'.str_replace(' ','+',$address['state']));
    $result_string = file_get_contents($url);
    $result = json_decode($result_string, true);
    if($result['status'] == 'OK'){
      $result1 = $result['results'][0]['geometry']['location'];
      return array('lat' => $result1['lat'],
		   'lng' => $result1['lng']);
    } else {
      return false;
    }
  }
}


//input Associative Array, that contains key 'lat' and 'lng'

// $lnglat input template:
// $lnglat = array( "lat" => lat, "lng" => lng );

function getLocation($lnglat){
  $url = "http://maps.google.com/maps/api/geocode/json?latlng=";
  
  $url = $url.urlencode($lnglat['lat']).','.urlencode($lnglat['lng']);
  echo $url;
  $result_string = file_get_contents($url);
  $result = json_decode($result_string, true);
  return $result['results'][0]['address_components'];
}

?>