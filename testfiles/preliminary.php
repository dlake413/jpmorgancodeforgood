<?php

//ini_set("display_errors",1);
//error_reporting(-1);

include 'PreliminaryResults.php';
session_start();



$results = findReleventTrials($_SESSION['age'], $_SESSION['PD'], $_SESSION['range'], $_SESSION['ZIP']);
$vol_id = $_SESSION['vol_id'];
?>

<!DOCTYPE html>
<html>
  <head>
    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
    <meta name='viewport' content='width=device-width,initial-scale=1.0'>
    <link href = 'css/bootstrap.css' rel='stylesheet'>
    <link href = 'css/styles.css' rel='stylesheet'>
    <style>
    #map-canvas{
      height:300px;
      width:300px;
     } 
    .labels{
     }
     body {
       background-color:lightgrey;
     }
    </style>

  </head>

<div class = "logo" align="center">
<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
</div>
<br>
  <body>
    <center>
      <!-- This DIV is going to be our map -->
      <div id='map-canvas'></div>
      <script type='text/javascript'>
       
       //This is where we mark out Trials near by.
       
       //Set global Variable maps
       var map = new google.maps.Map(document.getElementById('map-canvas'), {zoom:12, center: centerlat});
       
       //addMarker: put marker on the map based on the lat/lang input
       //TODO:: make it crate MarkerWithLabel instead of Marker to print the name of the Trials
       function addMarker(lat, lang, name){
	 var marker = new google.maps.Marker(
	   {position: new google.maps.LatLng(lat,lang),
	    map:map,
	   });
       }
       
       //Calls php array into results.
       var results = <?php echo json_encode($results); ?>;
       var coordinates = [];

       //I added the center position,  which is a user location, at the end of the results array.
       var centerlat = new google.maps.LatLng(results[results.length-1]['lat'], results[results.length-1]['lng']);
       
       /*
       Structure of results:
	  RESULTS = [
	      {trial:{
	                NAME:...,
	                STREET:...,
	                CITY:...,
	                STATE:...,
	                ZIP:...,
	                lng:...,
	                lat:...,
	             }
	      }
	  .
	  .
	  .
	  ]
       */
       
       for (var i = 0; i < results.length-1; i++){
	 var obj = results[i]['trial'];
	 addMarker(obj['lat'],obj['lng'],obj['NAME']);
       }
       
       map.setCenter(centerlat);
       
      </script>
      <br>
      <?php
      echo "These are Trials Near You!<br>";
      
      foreach ($results as $row){
	if(isset($row['trial'])){
	  echo $row['trial']['NAME'].' '.
	       $row['trial']['STREET'].' '.
	       $row['trial']['CITY'].' '.
	       $row['trial']['STATE'].' '.
	       $row['trial']['ZIP'].' DISTANCE '.
	       $row['distance'].' Miles<br>';
	}
      }
      
      ?>
      <br>
      <form action="Q1.php" method="post">
	<input type='submit' class="btn btn-default btn-lg btn-block" 
	       value="Would you like to see if you qualify?" 
	       name="moreinfo"></input><br>
      </form>
      <?php 
      if(isset($_POST['moreinfo'])){
	$_SESSION['vol_id']=$vol_id;
	header('Location : http://ec2-107-22-23-216.compute-1.amazonaws.com/testfiles/Q1.php');
      }
      ?>
    </center>
  </body>
</html>