<!--Home page once you have logged in, allows you to look at the trials that fit 
your qualifications, edit profile, and continue filling out the survey -->
<?php
include 'PreliminaryResults.php';
$results = findReleventTrials(20, 'Y', 50, 20904);
?>
<div class = "logo" align="center">
<img alt="" src="logo.png" width="330" height="90" class="imagestyle" /></a>
</div><BR><BR>
<!DOCTYPE html>
<html>
  <head>
    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
    <style>
    #map-canvas{
      height:300px;
      width:300px;
     } 
    .labels{
     }
     .pic{
		text-align: center;
		-moz-border-radius:40px;
		-webkit-border-radius:40px;
		border-radius:40px;
		border:1px solid black; 
		width:700px;
	}
    </style>
  </head>
  <body>
  	<center>
    <div id='map-canvas'></div>
    <script type='text/javascript'>
     function addMarker(lat, lang, name){
       var marker = new google.maps.Marker(
	 {position: new google.maps.LatLng(lat,lang),
	  map:map,
	 });
     }

     var results = <?php echo json_encode($results); ?>;
     var coordinates = [];
     var centerlat = new google.maps.LatLng(results[results.length-1]['lat'], results[results.length-1]['lng']);
     var map = new google.maps.Map(document.getElementById('map-canvas'), {zoom:12, center: centerlat});
     for (var i = 0; i < results.length-1; i++){
       var obj = results[i]['trial'];
       addMarker(obj['lat'],obj['lng'],obj['NAME']);
       var coordinate = {lat:obj['lat'],lng:obj['lng'],trial:results[i]['trial']};
       coordinates.push(coordinate);
     }

     map.setCenter(centerlat);

    </script>
	<BR>
	<?php echo "These are Trials Near You!". '<br>'; ?>
    <BR>
    <?
    foreach ($results as $row){
      if(isset($row['trial'])){
	echo $row['trial']['NAME'].' '.
	     $row['trial']['STREET'].' '.
	     $row['trial']['CITY'].' '.
	     $row['trial']['STATE'].' '.
	     $row['trial']['ZIP'].' DISTANCE: '.
	     $row['distance'].' miles<br><br>';
      }
    }
    ?>
    <BR>
</center>
  </body>
</html>

<center>
<?php
session_start();
$vol_id = $_SESSION['vol_id'];
$folder = $_SESSION['folder'];
if($_SESSION['permission'] == 0){
	header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Fox Trial Finder</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "css/bootstrap.css" rel = "stylesheet">
		<link href = "css/styles.css" rel = "stylesheet">
</head>

<body>
<style>
  body {background-color:lightgrey}
</style>

  <div class="container">
    <div class="row"> 
      <div class="col-md-4"></div>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
<input type="submit" class="btn btn-default btn-lg btn-block" value="Tell us more about yourself!" name="moreinfo"><BR>
<input type="submit" class="btn btn-default btn-lg btn-block" value="Edit your profile" name="edit"><BR>
<input type="submit" class="btn btn-default btn-lg btn-block" value="Log out" name="out"><BR>
</form>
 </div>
 </div>
 </div>
<?php
if(isset($_POST['moreinfo'])){
	$_SESSION['vol_id'] = $vol_id;
	header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Q1.php');
}
if(isset($_POST['edit'])){

}
if(isset($_POST['out'])){
	$_SESSION['permission'] = 0;
	header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Login.php');
}
?>
</body>

</html>