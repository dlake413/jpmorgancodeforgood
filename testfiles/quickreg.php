<!-- Allows user to register with only email address-->
<?php
session_start();
//If user is registering they are automatically logged in when they register
$_SESSION['permission'] = 1;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register Now!</title>
		<!-- Load bootstrap -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "css/bootstrap.css" rel = "stylesheet">
		<link href = "css/styles.css" rel = "stylesheet">

		<script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
		<script src = "js/bootstrap.js"></script>
</head>
<style>
	.pic{
		margin: auto;
    	display: inline-block;
		text-align: left;
		font-size: 12pt;
		width: 300px;
	}
</style>
<center>
<!-- Fox Trial Finder logo -->
<div>
<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
</div>
<BR>
<?php
//Including functions for mySQL
include_once("../settings/DAO.php");

//Keeping variables so they can display in form in case of error
$email = $_POST['email'];
?>

<body>

<!-- Form for entering email-->
<div class="pic" >
<center>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" 
class="navbar-form navbar-left">
  <div class="form-group">
    Email: <input type="email" class="form-control" placeholder="Email" 
    name = "email" value = "<?echo $email ?>" required>
  </div><BR><BR>
  <input type="submit" value="Register Now" name="register" class="btn-lg btn-default"> <input type="submit" value="Back to Login Page" name="login" class="btn-lg btn-default"><BR><BR>
<center>
</form>
<?php
//Keep folder in case we move code from testfiles folder
$folder="/testfiles";
$_SESSION['folder'] = $folder;

//If the register button is clicked
if(isset($_POST['register'])){ 
	$vol_id = createVolunteer('', '', $email, '', '', '', '', '', '');
	echo "You have been registered, expect an email from us soon including your password!";
}
if(isset($_POST['login'])){ 
    header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com/testfiles/Login.php');
}