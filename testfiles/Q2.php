<!--Gets users procedures -->
<?php
include_once("../settings/DAO.php");
session_start();
$folder=$_SESSION['folder'];
if($_SESSION['permission'] == 0){
	header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Login.php');
}
$vol_id = $_SESSION['vol_id'];
$surgery1 = $_POST['surgery1'];
$surgery2 = $_POST['surgery2'];
$surgery3 = $_POST['surgery3'];
$surgery4 = $_POST['surgery4'];
?>
<!-- 
	Question 3
-->
<!DOCTYPE html>
<html>
<head>


	<div class = "logo" align="center">
<img alt="" src="logo.png" width="320" height="80" class="imagestyle" /></a>
</div>

<style>
  .pic{
    margin: auto;
    display: inline-block;
    text-align: left;
    font-size: 12pt;
    width: 300px;
  }
</style>

<meta charset="UTF-8">
<title>Title of the document</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "css/bootstrap.css" rel = "stylesheet">
		<script src = "http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
    	<script src = "js/bootstrap.js"></script>
</head>
<body>
<center>
	<BR>
<div class = "pic">
	Have you undergone any one of the following surgeries? <BR> <BR>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
<div class="checkbox">
    <label>
    	<input type="checkbox" name = "surgery1" value="1" > Deep Brain Stimulation <BR>
		<input type="checkbox" name = "surgery2" value="2" > Infusions into Skin  <BR>
		<input type="checkbox" name = "surgery3" value="3" > Infusions into Stomach <BR>
		<input type="checkbox" name = "surgery4" value="4" > Other Neurosurgery <BR>
    </label>
  </div>
<BR><input type="submit" value="Save and Exit" name="save" class="btn-lg btn-default">  <input type="submit" value="Next" name="next" class="btn-lg btn-default">
</form>
</div>
</center>

<?php
	
	if($surgery1 != ''){
		createVOL_PROCEDURES($vol_id, $surgery1);
	}
	if($surgery2 != ''){
		createVOL_PROCEDURES($vol_id, $surgery2);
	}
	if($surgery3 != ''){
		createVOL_PROCEDURES($vol_id, $surgery3);
	}
	if($surgery4 != ''){
		createVOL_PROCEDURES($vol_id, $surgery4);
	}
	if(isset($_POST['next'])){ 
		header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/Q3.php');
	}
	if(isset($_POST['save'])){ 
		header('Location: http://ec2-107-22-23-216.compute-1.amazonaws.com'.$folder.'/FoxTrialFinder.php');
	}
?>


</body>

</html>